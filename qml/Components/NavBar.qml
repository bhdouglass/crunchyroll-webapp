import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as Lomiri

ToolBar {
    property string url
    property bool landscape: false

    property QtObject colors: QtObject {
        readonly property color divider: '#f47521'
        readonly property color highlight: '#f47521'
        readonly property color text: '#363231'
        readonly property color background: '#ffffff'
    }

    GridLayout {
        anchors {
            fill: parent
            leftMargin: landscape ? 0 : units.gu(1)
            rightMargin: landscape ? 0 : units.gu(1)
            topMargin: landscape ? units.gu(6) : 0
            bottomMargin: landscape ? units.gu(6) : 0
        }

        columns: landscape ? 1 : 11
        rows: landscape ? 11 : 1

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Lomiri.Icon {
                    name: 'go-home'
                    color: (url == 'https://www.crunchyroll.com/') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://www.crunchyroll.com/'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Lomiri.Icon {
                    name: 'media-playlist'
                    color: (url == 'https://www.crunchyroll.com/home/queue') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://www.crunchyroll.com/home/queue'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Lomiri.Icon {
                    name: 'toolkit_input-search'
                    color: url.startsWith('https://www.crunchyroll.com/search') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://www.crunchyroll.com/search'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ToolButton {
                anchors.centerIn: parent
                width: units.gu(4)
                height: units.gu(4)

                contentItem: Lomiri.Icon {
                    name: 'settings'
                    color: url.startsWith('https://www.crunchyroll.com/acct') ? colors.highlight : colors.text
                }

                onClicked: url = 'https://www.crunchyroll.com/acct'

                // Don't show the pressed background
                background: Rectangle { visible: false }
            }
        }
    }

    background: Rectangle {
        color: colors.background

        Rectangle {
            visible: !landscape
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            height: units.dp(2)
            color: colors.divider
        }

        Rectangle {
            visible: landscape
            anchors {
                bottom: parent.bottom
                right: parent.right
                top: parent.top
            }

            width: units.dp(2)
            color: colors.divider
        }
    }
}
