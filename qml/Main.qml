import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3 as Lomiri
import Morph.Web 0.1
import QtWebEngine 1.7
import QtSystemInfo 5.5

import "Components"

ApplicationWindow {
    id: root
    visible: true
    color: webview.isFullScreen ? '#000000' : Suru.backgroundColor

    width: units.gu(45)
    height: units.gu(75)

    property bool landscape: width > height

    WebContext {
        id: webcontext
        httpUserAgent: 'Mozilla/5.0 (Linux; Ubuntu 20.04 like Android 9) AppleWebKit/537.36 Chrome/87.0.4280.144 Mobile Safari/537.36'
        offTheRecord: false

        userScripts: [
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentCreation
                worldId: WebEngineScript.MainWorld
                name: "QWebChannel"
                sourceUrl: "customcss.js"
            }
        ]
    }

    WebView {
        id: webview
        anchors {
            top: parent.top
            left: (isFullScreen || !landscape) ? parent.left : navBar.right
            right: parent.right
            bottom: (isFullScreen || landscape) ? parent.bottom : navBar.top
        }

        // TODO error page
        context: webcontext
        url: 'https://www.crunchyroll.com/'

        onUrlChanged: navBar.url = url

        onFullScreenRequested: function(request) {
            if (request.toggleOn) {
                root.showFullScreen();
            }
            else {
                root.showNormal();
            }
            request.accept();
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;

            if (!url.match('(http|https)://(www|beta|sso).crunchyroll.com/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    NavBar {
        height: landscape ? parent.width : units.gu(6)
        width: landscape ? units.gu(6) : parent.width

        id: navBar
        visible: !webview.isFullScreen
        anchors {
            top: landscape ? parent.top : undefined
            left: parent.left
            right: landscape ? undefined : parent.right
            bottom: parent.bottom
        }

        landscape: root.landscape
        onUrlChanged: webview.url = url
    }

    ScreenSaver {
        screenSaverEnabled: !Qt.application.active || !webview.recentlyAudible
    }

    Connections {
        target: Lomiri.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('crunchyroll.com') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }

}
